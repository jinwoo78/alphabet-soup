import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.annotation.PreDestroy;

public class Demo {
   public static void main(String[] args) {
      AlphabetSoup alphabetSoup = new AlphabetSoup();

      // parse and get input from file
      int lineCount = 0;
      try (Scanner scanner = new Scanner(new File(args[0]))) {
         while (scanner.hasNextLine()) {
            String line = scanner.nextLine();

            if (lineCount == 0) {
               alphabetSoup.assignBoundaries(line.split("x"));
            } else {
               if (lineCount <= alphabetSoup.getRowBoundary()) {
                  alphabetSoup.addGridData(lineCount - 1, line.replaceAll("\\s", ""));
               } else {
                  alphabetSoup.addKey(line);
               }
            }

            lineCount++;
         }
      } catch (FileNotFoundException e) {
         e.printStackTrace();
      }

      alphabetSoup.findAndPrintKeys();
   }
}

class AlphabetSoup {

   public enum Direction {
      UP,
      UPLEFT,
      LEFT,
      DOWNLEFT,
      DOWN,
      DOWNRIGHT,
      RIGHT,
      UPRIGHT
   }

   private int rowBoundary;

   private int columnBoundary;

   private char[][] charArray;

   private final Queue<String> keyList = new LinkedList<>();

   private final ReadWriteLock lock = new ReentrantReadWriteLock();

   private final ExecutorService executor = Executors.newCachedThreadPool(r -> {
      final Thread t = new Thread(r);
      t.setDaemon(true);
      return t;
   });

   @PreDestroy
   public void destroy() {
      executor.shutdown();
   }

   public void assignBoundaries(String[] boundaryArray) {
      if (boundaryArray.length == 2) {
         rowBoundary = Integer.parseInt(boundaryArray[0]);
         columnBoundary = Integer.parseInt(boundaryArray[1]);
         charArray = new char[rowBoundary][columnBoundary];
      } else {
         System.err.println("Invalid boundary data!");
         System.exit(1);
      }
   }

   public void addGridData(int row, String data) {
      if ((row >= 0 && row < rowBoundary) && (data != null && !data.isEmpty())) {
         for (int col = 0; col < columnBoundary; col++) {
            charArray[row][col] = data.charAt(col);
         }
      } else {
         System.err.println("Invalid grid data!");
         System.exit(1);
      }
   }

   public void addKey(String key) {
      if (key != null && !key.isEmpty()) {
         keyList.add(key);
      } else {
         System.err.println("Invalid key: " + key);
      }
   }

   public int getRowBoundary() {
      return rowBoundary;
   }

   public void findAndPrintKeys() {
      boolean first = true;

      while (!keyList.isEmpty()) {
         String key = keyList.poll();
         Coordinates begin = new Coordinates();
         Coordinates end = new Coordinates();

         boolean found = searchKey(key.replaceAll("\\s", ""), begin, end);

         // if found, print the result
         if (found) {
            if (first) {
               first = false;
            } else {
               System.out.println();
            }
            System.out.print(key + " " + begin + " " + end);
         }
      }
   }

   private boolean searchKey(String key, Coordinates begin, Coordinates end) {
      if (key == null || key.isEmpty() || begin == null || end == null) {
         System.err.println("Invalid input in searchKey!");
         return false;
      }

      for (int i = 0; i < rowBoundary; i++) {
         for (int j = 0; j < columnBoundary; j++) {
            if (charArray[i][j] == key.charAt(0)) {
               begin.setRow(i);
               begin.setColumn(j);
               end.copyOf(begin);

               final List<Future<Boolean>> futureList = new ArrayList<>();
               Arrays.stream(Direction.values()).forEach(direction -> futureList.add(executor.submit(() -> matchKeyCharByChar(key, end, direction))));

               boolean anyFound = false;
               try {
                  for (Future<Boolean> future : futureList) {
                     if (future != null) {
                        anyFound = anyFound || future.get();
                     }
                  }
               } catch (Exception e) {
                  e.printStackTrace();
               }

               if (anyFound) {
                  return true;
               }
            }
         }
      }

      return false;
   }

   private boolean matchKeyCharByChar(String key, Coordinates coordinates, Direction direction) {
      if (key == null || key.isEmpty() || coordinates == null || direction == null) {
         System.err.println("Invalid input in matchKeyCharByChar!");
         return false;
      }

      Coordinates tempCoordinates = new Coordinates(coordinates);
      for (char c : key.substring(1).toCharArray()) {
         if (moveOnceToDirection(tempCoordinates, direction)) {
            char other;
            lock.readLock().lock();
            try {
               other = charArray[tempCoordinates.getRow()][tempCoordinates.getColumn()];
            } finally {
               lock.readLock().unlock();
            }

            if (c != other) {
               return false;
            }
         } else {
            return false;
         }
      }
      coordinates.setRow(tempCoordinates.getRow());
      coordinates.setColumn(tempCoordinates.getColumn());

      return true;
   }

   private boolean moveOnceToDirection(Coordinates coordinates, Direction direction) {
      if (coordinates == null || direction == null) {
         System.err.println("Invalid input in moveOnceToDirection!");
         return false;
      }

      switch (direction) {
         case UP:
            coordinates.moveUp();
            break;
         case UPLEFT:
            coordinates.moveUpLeft();
            break;
         case LEFT:
            coordinates.moveLeft();
            break;
         case DOWNLEFT:
            coordinates.moveDownLeft();
            break;
         case DOWN:
            coordinates.moveDown();
            break;
         case DOWNRIGHT:
            coordinates.moveDownRight();
            break;
         case RIGHT:
            coordinates.moveRight();
            break;
         case UPRIGHT:
            coordinates.moveUpRight();
            break;
         default:
            System.err.println("Shouldn't reach here!");
            return false;
      }

      boolean rowCheck = coordinates.getRow() >= 0 && coordinates.getRow() < rowBoundary;
      boolean colCheck = coordinates.getColumn() >= 0 && coordinates.getColumn() < columnBoundary;
      return rowCheck && colCheck;
   }

}

class Coordinates {

   int row;

   int column;

   // default constructor
   public Coordinates() {
   }

   // copy constructor
   public Coordinates(Coordinates coordinates) {
      this.row = coordinates.getRow();
      this.column = coordinates.getColumn();
   }

   public void copyOf(Coordinates coordinates) {
      this.row = coordinates.getRow();
      this.column = coordinates.getColumn();
   }

   public void moveUp() {
      column--;
   }

   public void moveUpLeft() {
      row--;
      column--;
   }

   public void moveLeft() {
      row--;
   }

   public void moveDownLeft() {
      row--;
      column++;
   }

   public void moveDown() {
      column++;
   }

   public void moveDownRight() {
      row++;
      column++;
   }

   public void moveRight() {
      row++;
   }

   public void moveUpRight() {
      row++;
      column--;
   }

   public int getRow() {
      return row;
   }

   public void setRow(int row) {
      this.row = row;
   }

   public int getColumn() {
      return column;
   }

   public void setColumn(int column) {
      this.column = column;
   }

   @Override
   public String toString() {
      return row + ":" + column;
   }

}